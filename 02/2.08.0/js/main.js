/*
*    main.js
*    Mastering Data Visualization with D3.js
*    2.8 - Activity: Your first visualization!
*/

d3.json('./data/buildings.json').then(function(data){
    var svg = d3.selectAll("#chart-area").append("svg")
        .attr("width", 400)
        .attr("height", 400);
    
    var rs = svg.selectAll("rect").data(data);
    
    rs.enter().append("rect")
        .attr("x", function(d, i){
            return i * 40;
        })
        .attr("y", 10)
        .attr("width", 30)
        .attr("height", function(d, i){
            console.log(d, i);
            return d.height;
        })
        .fill("fill", "#888");
});